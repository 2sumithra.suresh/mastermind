import json
import requests
import random


class Sessions:
    def __init__(self, name, mode):
        self.url = "https://we6.talentsprint.com/wordle/game/"
        self.data = {"mode": mode, "name": name}
        self.mode = mode
        self.session = requests.Session()
        self.session.headers.update({'Content-Type': 'application/json'})
        response1 = self.session.post(self.url + "register", json=self.data)
        self.info = response1.json()

        data = {"id": self.info["id"], "overwrite": True}
        response2 = self.session.post(self.url + "create", json=data)

    def guess(self, guess_word):
        data = {"guess": guess_word, "id": self.info["id"]}
        response3 = self.session.post(self.url + "guess", json=data)
        return response3.json()

    def get_words(self):
        fp = open("5letters.txt", "r")
        words = fp.read().split('\n')
        return list(set(words))

    def play(self):
        words = self.get_words()
        
        if self.mode == "mastermind":
            feedback = 0
            while feedback != 5:
                word = random.choice(words)
                response = self.guess(word)
                feedback = response["feedback"]
                message = response["message"]
                print(f"Correct Letters: {feedback}. {message}")
                words.remove(word)
            if feedback == 5:
                print("You Won!")

        elif self.mode == "wordle":
            while True :
                word = random.choice(words)
                response = self.guess(word)
                print(response)
                if "exceeded" in response["message"] or "win" in response["message"]:
                    break
                  
    

MM1 = Sessions("sumithra", "wordle")
print(MM1.play())
